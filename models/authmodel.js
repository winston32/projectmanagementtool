var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
var Schema = mongoose.Schema;

var authSchema = new Schema({
    username : {
        type: String,
        index : true
    },
    email : {
        type: String
    },
    password : {
        type: String
    },
});


var authModel = mongoose.model('authusers', authSchema);

module.exports.model = authModel;


module.exports.createUser = function(user, callback) {
    user.save(callback);
}

module.exports.getUserById = function(id, callback) {
    authModel.findById(id, callback);
}

module.exports.getUser = function(user, callback) {
    authModel.findOne({'username' : user}, callback);
}

module.exports.passwordsMatch = function(dbpassword,candidatePassword, callback) {
    bcrypt.compare(candidatePassword, dbpassword,callback);
}