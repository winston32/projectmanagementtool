var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var session = require('express-session');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var passportHttp = require('passport-http');
var bcrypt = require('bcryptjs');
var authModel = require('./models/authmodel.js');
var moment = require('moment');

var app = express();

app.use(cors());


app.use(passport.initialize());
app.use(passport.session());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true
}));

passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    authModel.getUserById(id, function (err, user) {
        done(err, user);
    });
});

passport.use(new LocalStrategy(
    function (username, password, done) {
        authModel.getUser(username, function (err, user) {
            if (err) { return done(err); }
            if (!user) { return done(null, false); }
            authModel.passwordsMatch(user.password, password, function (err, match) {
                if (err) { return done(null, false); }
                if (match) { return done(null, user); } else {
                    return done(null, false);
                }
            })
        });
    }
));


app.use('*', function (req, res, next) {
    res.locals.user = req.user || null;
    next();
});


mongoose.connect("mongodb://localhost:27017/projectmanagement");
var Schema = mongoose.Schema;
var projectdetailsSchema = new Schema({ id: Number, ProjectName: String, Description: String, progress: String, StartDate: { type: Date }, EndDate: { type: Date }, status: String, type: String, parent: String }, { strict: false });
//var phaseDetailsSchema = new Schema({PhaseName:String,StartDate:Date,EndDate:Date},{strict:false})

//var resourceDetailsSchema = new Schema({ResourceName:String,ResourceId:String,ResourceDesignation:String,ManagerName:String},{strict:false})
//var resourceDetailsSchema = new Schema({ResourceName:String,ResourceId:String,ResourceDesignation:String,ManagerName:String,TaskDetails:[]},{strict:false})

var resourceDetailsSchema = new Schema({ IsManagerFlag: Boolean, ResourceName: String, ResourceId: String, ResourceDesignation: String, resourceBandwidth: Number, ManagerName: String, TaskDetails: [] }, { strict: false })
var TaskDetails = new Schema({ Id: Number, ProjectId: String, bandwidth: Number, text: String, Start: { type: Date }, End: { type: Date } }, { strict: false })

var linkDetailsSchema = new Schema({ source: String, target: String, type: String, id: Number }, { strict: false })
var linkDetailsModel = mongoose.model('linkdetails', linkDetailsSchema);
var projectDetailsModel = mongoose.model('projectdetails', projectdetailsSchema);
var resourceDetailsModel = mongoose.model('resourcedetails', resourceDetailsSchema);
//var phaseDetailsModel = mongoose.model("")


app.listen(1337);

app.get('/test', function (req, res) {
    res.json({ 'NAME': 'hello' });
    res.end();
});

app.post('/project', function (req, res) {


    var jsonBody = req.body;

    //console.log('priority is : ' + jsonBody.projectPriority)
    jsonBody.text = req.body.projectName;
    jsonBody.start_date = moment(req.body.projectStartDate, "MM/DD/YYYY").format("DD-MM-YYYY")
    jsonBody.progress = 0;
    jsonBody.open = true;
    jsonBody.id = Math.floor((Math.random() * 999) + 1);
    jsonBody.progress = req.body.progress;
    jsonBody.isArchive = false;


    if (jsonBody.projectPriority == 1) {
        //console.log('inside ...');
        jsonBody.color = 'Orange';
        jsonBody.textColor = 'Khaki';
    } else if (jsonBody.projectPriority == 2) {
        //console.log('inside priority blovk of : ' + jsonBody.projectPriority);
        jsonBody.color = 'LimeGreen';
        jsonBody.textColor = 'Khaki';
    }
    //jsonBody.progressColor = 'Orange';
    //process duration
    var startDateFormat = moment(req.body.projectStartDate, "MM/DD/YYYY");
    var endDate = moment(req.body.projectEndDate, "MM/DD/YYYY");
    var durationOfDays = endDate.diff(startDateFormat, 'days');
    jsonBody.duration = durationOfDays;
    delete jsonBody.projectName;
    delete jsonBody.projectDesc;
    delete jsonBody.projectBrand;
    delete jsonBody.projectStartDate;
    delete jsonBody.projectEndDate;
    var projectDetails = projectDetailsModel(jsonBody);
    projectDetails.save(function (err, data) {
        if (err) {
            console.log('some error occured');
        }
        res.send(data);
    })
});

app.post('/link', function (req, res) {
    var jsonBody = req.body;
    var linkDetails = new linkDetailsModel(jsonBody);
    linkDetails.save(function (err, data) {
        if (err) console.log('error');
        res.send('success');
    })

})

app.post('/subTask', function (req, res) {

    var jsonBody = req.body;
    //console.log("receievd data is : " + JSON.stringify(jsonBody));
    //console.log("Identity : " + jsonBody.identity);
    if (jsonBody.identity == "SUBTASK") {
        var actualJSON = {};
        actualJSON.id = jsonBody.id;
        actualJSON.text = jsonBody.text;
        actualJSON.start_date = moment(jsonBody.start_date, "YYYY/MM/DD").format("DD-MM-YYYY")
        actualJSON.duration = jsonBody.duration;
        actualJSON.progress = jsonBody.progress;
        actualJSON.manager = jsonBody.manager;
        //actualJSON.assignedTo = jsonBody.assignedTo;
        //actualJSON.assignedTo = '';
        actualJSON.assignees = [];
        actualJSON.priority = jsonBody.priority; // need to work out
        actualJSON.open = true;
        actualJSON.parent = jsonBody.parent;
        actualJSON.isArchive = false;
        if (jsonBody.priority == 1) {
            //console.log('inside ...');
            actualJSON.color = 'Orange';
            actualJSON.textColor = 'Khaki';
        } else if (jsonBody.priority == 2) {
           //console.log('inside priority blovk of : ' + jsonBody.projectPriority);
            actualJSON.color = 'LimeGreen';
            actualJSON.textColor = 'Khaki';
        }

        var projectDetails = projectDetailsModel(actualJSON);
        projectDetails.save(function (err, data) {
            //console.log("save subtask project----");
            if (err) {
                console.log('some error occured'+err);
            }
            res.send(data);
        })
    }
    else if (jsonBody.identity == "RESOURCE") {
        //console.log("assigning resource-----");
        var jsonBody = req.body;
        var actualJSON = {};
        actualJSON.id = jsonBody.id;
        actualJSON.text = jsonBody.text;
        actualJSON.Start = moment(jsonBody.start_date, "YYYY/MM/DD").format("DD-MM-YYYY")
        actualJSON.End = moment(jsonBody.end_date, "YYYY/MM/DD").format("DD-MM-YYYY")
        actualJSON.duration = jsonBody.duration;
        actualJSON.bandwidth = jsonBody.bandwidth;
        actualJSON.priority = jsonBody.priority;
        actualJSON.open = true;
        actualJSON.parent = jsonBody.parent;
        actualJSON.ProjectName = jsonBody.project;
        //  var resourceBandwidth=jsonBody.resourceBandwidth;//+jsonBody.bandwidth;
        //console.log("actualJSON.project--" + actualJSON.ProjectName +"--"+jsonBody.text);
        if (jsonBody.parent) {

            //console.log("resourceBandwidth---"+parseInt(jsonBody.bandwidth) +jsonBody.parent);
            resourceDetailsModel.findOneAndUpdate({ _id: jsonBody.parent },
                { $addToSet: { TaskDetails: actualJSON }}, function (err, data) {
                 //   //console.log("resource saved---");
                    if (err) console.log("error occured---" + err);
                    res.send(data);
                })

        resourceDetailsModel.findByIdAndUpdate({ _id: jsonBody.parent },  { $inc: { resourceBandwidth:+parseInt(jsonBody.bandwidth)} }, function (err, data) {
            if (err) console.log(err);
           // break;
             res.send(data);
           //console.log('data from updated resource details is : ' + data)
           ;
        })
        }
    }
});

app.delete('/delete/:id', function (req, res) {
    //console.log('id to be deleted is : ' + req.params.id);
    projectDetailsModel.remove({ "_id": req.params.id }, function (err, data) {
        if (err) console.log('error');
        res.send('success');
    })
})
app.put('/archive/:id', function (req, res) {
    var id = req.params.id;
    //console.log('id to be archived is : ' + req.params.id);
    projectDetailsModel.update({ id: req.params.id }, { isArchive: true,color:"Blue",textColor:"Black" }, {multi: true}, function (err, data) {
        if (err) console.log('error' + err);

    })
    projectDetailsModel.update({ "parent": id }, { isArchive: true,color:"Blue",textColor:"Black" },  {multi: true},function (err, data) {
        if (err) console.log('error' + err);

    })
    
      //    resourceDetailsModel.findOneAndUpdate({ TaskDetails:{ProjectId: req.body.id }},{$pull:{TaskDetails: {ProjectName: req.body.text}}},function(err,data){
            resourceDetailsModel.update({},{$pull:{TaskDetails: {ProjectId: id}}}, {multi: true},function(err,data){
        if(err) console.log('error'+err);
       
    });

  

    res.send('success');
})

//deletelink

app.delete('/deletelink/:_id', function (req, res) {
    //console.log('link id to be deleted is : ' + req.params._id);
    linkDetailsModel.remove({ "_id": req.params._id }, function (err, data) {
        if (err) console.log('error');
        res.send('success');
    })
})

app.put('/updatelink', function (req, res) {
    //console.log('received date to update the link is :' + JSON.stringify(req.body));

})

app.put('/updatetask', function (req, res) {
    //console.log('received data is : ' + JSON.stringify(req.body) + "Progress---" + req.body.progress);
    var idToBeUpdated;
    if (!req.body._id) {
        //console.log('inside child task update flow');
        projectDetailsModel.find({ 'id': req.body.id }, function (err, data) {
            //console.log("got data for  mongo is : " + data);
            idToBeUpdated = data._id;
        })
    } else {
        idToBeUpdated = req.body._id;
    }
    var actualJSONToBeUpdated = {};
    if (req.body.priority == 1) {
        actualJSONToBeUpdated.color = 'Orange';
        actualJSONToBeUpdated.textColor = 'Khaki';
    } else if (req.body.priority == 2) {
        actualJSONToBeUpdated.color = 'LimeGreen';
        actualJSONToBeUpdated.textColor = 'Khaki';
    } else if (req.body.priority == 3) {
        actualJSONToBeUpdated.color = 'Default';
        actualJSONToBeUpdated.textColor = 'Default';
    }

    if (req.body.progress == 1) {
        actualJSONToBeUpdated.color = 'LightSteelBlue';
    }
    actualJSONToBeUpdated.duration = req.body.duration;
    actualJSONToBeUpdated.start_date = moment(req.body.start_date, "YYYY-MM-DD").format("DD-MM-YYYY"); //moment(jsonBody.start_date,"YYYY/MM/DD").format("DD-MM-YYYY")
    actualJSONToBeUpdated.text = req.body.text;
    actualJSONToBeUpdated.progress = Math.round(req.body.progress * 10) / 10;//Math.round( req.body.progress * 10 ) / 10
  actualJSONToBeUpdated.manager=req.body.manager;
    projectDetailsModel.findOneAndUpdate({ '_id': idToBeUpdated }, actualJSONToBeUpdated, function (err, data) {
        if (err) console.log('error');
        //console.log('response is : ' + data);

    })
    if (req.body.progress == 1 && req.body.parent == 0) {
        //console.log('complete child project is : ' + req.body.id);

        projectDetailsModel.findOneAndUpdate({ 'parent': req.body.id }, { $set: { progress: 1 } }, function (err, data) {
            if (err) console.log('error');
        })
      //    resourceDetailsModel.findOneAndUpdate({ TaskDetails:{ProjectId: req.body.id }},{$pull:{TaskDetails: {ProjectName: req.body.text}}},function(err,data){
            resourceDetailsModel.update({},{$pull:{TaskDetails: {ProjectName: req.body.text}}}, {multi: true},function(err,data){
        if(err) console.log('error'+err);
       
    });
}
 else if (req.body.progress == 1 && req.body.parent != 0) {
        //console.log('complete parent project is : ' + req.body.id);

        projectDetailsModel.findOneAndUpdate({ 'parent': req.body.id }, { $set: { progress: 1 } }, function (err, data) {
            if (err) console.log('error');
        })
        //  resourceDetailsModel.findOneAndUpdate({ TaskDetails:{ProjectId: req.body.id }},{$pull:{TaskDetails: {text: req.body.text}}},function(err,data){
             resourceDetailsModel.update({},{$pull:{TaskDetails: {text: req.body.text}}}, {multi: true},function(err,data){
        if(err) console.log('error'+err);
       
    });
    }
    res.send('success');
})

app.post('/resource', function (req, res) {
    //console.log('resource data is : ' + JSON.stringify(req.body));
    var jsonBody = req.body;
    jsonBody.IsManagerFlag = false;
    jsonBody.resourceBandwidth = 0;
     jsonBody.parent = 0;
    if (!jsonBody.projectIds) {
        jsonBody.TaskDetails = [];
    }
    if (jsonBody.resourceManager == null ||jsonBody.resourceManager=="") {
        jsonBody.IsManagerFlag = true;
        jsonBody.resourceManager="Brent";
    }

    jsonBody.resourceCountOfProjects = 2;
    var resourceDetails = resourceDetailsModel(jsonBody);
    resourceDetails.save(function (err, data) {
        if (err) {
            console.log('some error occured');
        }
        res.send(data);
    })
});

app.post('/phase', function (req, res) {
    var projectID = req.body.parent;
    //delete req.body._id;
    var phaseDetailsInput = req.body;
    projectDetailsModel.save(phaseDetailsInput, function (err, data) {
        if (err) console.log("some error occured");
        res.send(data);
    })
})

app.put('/project', function (req, res) {
    var projectId = req.body._id;
    var options = { multi: false };
    projectDetailsModel.update(projectId, req.body, options, function (err, numAffected) {
        if (err) console.log('some error');
        res.send(numAffected);
    })
});

app.put('/assignproject', function (req, res) {
    ////console.log('received data for assign project is :' + JSON.stringify(req.body));
    projectDetailsModel.update({ id: req.body.subTask.ProjectId }, { $addToSet: { assignees: req.body.userName } }, function (err, data) {
        if (err) console.log('error');
      //  //console.log('data from updated project details is ' + data);
    })

    projectDetailsModel.update({ id: req.body.subTask.Id }, { $addToSet: { assignees: req.body.userName } }, function (err, data) {
        if (err) console.log('error');
       //console.log('data from updated project details is ' + data);
    })

    if ((req.body.subTask && req.body.subTask != null)) {
        //console.log(req.body.subTask.bandwidth);
        resourceDetailsModel.findByIdAndUpdate({ _id: req.body.userId }, { $addToSet: { TaskDetails: req.body.subTask } }, function (err, data) {
            if (err) console.log(err);
          //  //console.log('data from updated resource details is : ' + data)

        })
         resourceDetailsModel.findByIdAndUpdate({ _id: req.body.userId },  { $inc: { resourceBandwidth:req.body.subTask.bandwidth} }, function (err, data) {
            if (err) console.log(err);
           //console.log('data from updated resource details is : ' + data)

        })
    }
    
    res.send('success');

})

app.get('/getAllProjectsIdsByuserid/:userid', function (req, res) {
    //console.log("user id is :" + req.params.userid);
    resourceDetailsModel.findById({ _id: req.params.userid }, (err, data) => {
        if (err) console.log(err);
        res.send(data);
    });



    //res.send('done')
})

app.get('/getAllSubTasksByProjectId/:id', function (req, res) {
    //console.log("recieved data in htting sub tasks is : " + req.params.id);
    projectDetailsModel.find({ "parent": req.params.id }, function (err, data) {
        if (err) console.log('error')
        res.send(data);
    });
})

app.get('/gettempAllSubTasksByProjectId', function (req, res) {
    //  //console.log("recieved data in htting sub tasks is : " + req.params.id) ;
    projectDetailsModel.find({ "parent": { $gt: 0 },'isArchive': false, 'progress': { $lt: 1, $gte: 0 } }, function (err, data) {
        if (err) console.log('error')
        res.send(data);
    })
})

app.put('/unassignproject/', function (req, res) {
   //console.log('inside unassign project' +req.body);
var resourcebandwidthTobeReduced;
   
    resourceDetailsModel.update({ _id: req.body.userId }, { $pull: { 'TaskDetails': { Id: req.body.taskId } } }, function (err, data) {
     //   //console.log('============1===================');
        if (err) console.log('error');
   
    })
   
     
    projectDetailsModel.update({ id: req.body.taskId }, { $pull: { 'assignees': req.body.userName } }, function (err, data) {
         //console.log('============2===================');
        if (err) console.log('err');

    })

    projectDetailsModel.findOneAndUpdate({ id: req.body.projectid }, { $pull: { 'assignees': req.body.userName } }, function (err, data) {
        //console.log('============3===================');
        if (err) console.log('err');

    })
       
       
            resourceDetailsModel.findByIdAndUpdate({ _id: req.body.userId },  { $inc: { resourceBandwidth:-parseInt(req.body.bandwidth)} }, function (err, data) {
           ////console.log('============5===================');
            if (err) console.log(err);
          //  //console.log('data from updated resource details is : ' + data)

        })  
      
    res.send('success');
})

app.get('/search/:searchValue', function (req, res) {
    var searchValue = req.params.searchValue;
   //console.log('value to be searched is : ' + searchValue);
    if (searchValue != 'all') {
        resourceDetailsModel.find({ $or: [{ resourceName: { $regex: searchValue, $options: "i" } }, { resourceManager: { $regex: searchValue, $options: "ig" } }, { resourceSkills: { $regex: searchValue, $options: "ig" } }, { resourceDesignation: { $regex: searchValue, $options: "ig" } }] }, function (err, data) {
            res.send(data);
        })
    } else {
        resourceDetailsModel.find({}, function (err, data) {
            if (err) console.log('error');
            res.send(data);
        })
    }

})

app.get('/getAllProjectsIds/:ids', function (req, res) {
    //console.log("ids of projects is : " + req.params.ids);
    var json = JSON.parse(req.params.ids);
    var pIdsArray = json.pids;
    //console.log('pida array is ' + pIdsArray);
    projectDetailsModel.find({ id: { $in: pIdsArray } }, function (err, data) {
        res.send(data);
    })
})

function processData(data) {
    //console.log('process data is : ' + data);
    var json = JSON.parse(data);
    //console.log('json is :' + json);
    //console.log(json.projectIds);
    var jsonArray = [];
    var count = 0;
    var projectIdsLength = json.projectIds.length;
    //console.log('lenght is : ' + projectIdsLength)
    json.projectIds.forEach(function (element, index) {
        count++;
        //console.log(element);
        projectDetailsModel.find({ $or: [{ 'id': element }, { 'parent': element }] }, function (err, data) {
            jsonArray.push(data);
            //console.log('count is ' + count)
            if (projectIdsLength == count) {
                //console.log("yes equal");
                return jsonArray;
            }
        })
    }, this);



}

app.get('/findprojectbyid/:id', function (req, res) {
    var projectID = req.params.id;
    projectDetailsModel.find({ _id: projectID }, function (err, data) {
        if (err) console.log('some error occured');
        res.send(data);
    })
});

app.get('/getAllParentProjects', function (req, res) {
    //console.log("/getAllParentProjects------------");
    projectDetailsModel.find({ 'parent': 0, 'isArchive': false, 'progress': { $lt: 1, $gte: 0 } }, function (err, data) {
        if (err) console.log('error');
        res.send(data);
    })
})
app.get('/getAllProjects', function (req, res) {
    projectDetailsModel.find({}, function (err, data) {
        if (err) console.log('some error occured'+err);
        res.send(data);
    });
});
app.get('/getArchivedProjects', function (req, res) {
    projectDetailsModel.find({ "isArchive": true }, function (err, data) {
        if (err) console.log('some error occured'+err);
        res.send(data);
    });
});
app.get('/getInprogressProjects', function (req, res) {
    projectDetailsModel.find({ progress: { $gte: "0",$lt:"1"}, 'isArchive': false }, function (err, data) {
        if (err) console.log('some error occured'+err);
        res.send(data);
    });
});
app.get('/getCompletedProjects', function (req, res) {
  //  //console.log("/getCompletedProjects------------");
    // var parent;
    projectDetailsModel.find({ progress: "1" }, function (err, data) {
       //  parent=data.parent; 
         ////console.log("/getCompletedProjects------------"+data+"parent");
        if (err) console.log('some error occured'+err); 
         res.send(data);
    })
    
});

app.get('/getAllResources', function (req, res) {
    resourceDetailsModel.find({}, function (err, data) {
        if (err) console.log('some error occured');
        res.send(data);
    });
});

app.get('/getAllLinks', function (req, res) {
    linkDetailsModel.find({}, function (err, data) {
        if (err) console.log('some error occured');
        res.send(data);
    });
});

app.post('/user/register', function (req, res, next) {
    //console.log(req.body.username);
    ////console.log('success');
    var username = req.body.username;
    var email = req.body.email;
    var pwd = req.body.pwd;
    // password hashing

    bcrypt.genSalt(10, function (err, salt) {
        bcrypt.hash(req.body.password, salt, function (err, hash) {
            // Store hash in your password DB. 
            var user = authModel.model({
                username: req.body.username,
                email: req.body.email,
                password: hash
            });

            authModel.createUser(user, function (err, data) {
                if (err) throw err;
                //res.send(data);
                res.send({ success: 'success' });
            })

        });
    });

});

app.post('/user/doLogin', passport.authenticate('local', { failureRedirect: '/user/invalidlogin' }), function (req, res) {
    res.locals.user = req.user || null;
    res.send({ success: true });
});

app.get('/user/invalidlogin', function (req, res) {
    res.send({ success: false });
});


//added by archana





/*methods used if using DAYPILOT SCHEDULER
app.put('/assignprojectfrom', function(req,res) {
    //console.log('received data for assign project is :' + JSON.stringify(req.body ));    
     var actualJSON={
         "userName":req.body.userName,
         "userID":req.body.resourceId
     }
    projectDetailsModel.update({id:req.body.ProjectId},{$addToSet: {assignees:req.body.userName}},function(err,data){
        if(err) //console.log('error');
       //console.log('data from updated project details is ' + data);
    }) 
  
  
 start_date = moment(req.body.Start,"MM/DD/YYYY").format("DD-MM-YYYY")
  end_date = moment(req.body.End,"MM/DD/YYYY").format("DD-MM-YYYY")
    var jsonData = {
        "Id":req.body.Id,
      "ProjectId" : req.body.ProjectId,
      "Start" : start_date,
      "End" : end_date,
      "text" :req.body.text,
      "bandwidth":req.body.bandwidth
    }   
    if(req.body._rid ) {        
   
    resourceDetailsModel.findOneAndUpdate({_id: req.body._rid}, 
    { $addToSet:{TaskDetails:jsonData}},function(err,data){
        if(err) //console.log(err);
        res.send(data);
    })
}
    
})


app.delete('/deleteSelectedAssignedProjectById/:id', function(req,res){
    //console.log('project id to be deleted is : ' + req.params.id);
       var result = [];
     result= req.params.id.split(/\|/);
    if(req.params.id.indexOf("TASK") > -1) {
    resourceDetailsModel.findOneAndUpdate({_id:result[2]},{$pull:{TaskDetails: {ProjectId: result[3]}}},function(err,data){
        if(err) //console.log('error'+err);
        res.send('success');
    });

}
else if(req.params.id.indexOf("RESOURCE") > -1){
     
     resourceDetailsModel.findOneAndUpdate({_id:result[2]},{$set:{TaskDetails:[]}},function(err,data){
        if(err) //console.log('error'+err);
        res.send('success');
    });
}
})

app.put('/moveSelectedProjectById', function(req,res) {
    //console.log('received data for move project is :' + JSON.stringify(req.body));
        var result = [];
     result= req.body.id.split(/\|/);
     start_date = moment(req.body.start,"YYYY-MM-DD").format("DD-MM-YYYY")   
     end_date = moment(req.body.end,"YYYY-MM-DD").format("DD-MM-YYYY")
     //console.log("end date---"+end_date);
    resourceDetailsModel.update({"TaskDetails.ProjectId" : result[2]}, {"$set" : {"TaskDetails.$.Start" : start_date,"TaskDetails.$.End" :end_date}}
   ,function(err,data){
        if(err) //console.log(err);
        res.send(data);
    })
}
    
)
*/

app.get('/getAllParentProjectsDrop', function (req, res) {
    projectDetailsModel.find({ 'parent': 0 }, { text: 1, _id: 1, id: 1 }, function (err, data) {
        //console.log("dropdown--"+data);
        if (err) console.log('error');
        res.send(data);
    })
})




app.get('/getResourcenameById/:id', function (req, res) {

    //console.log("recieved data resourceId : " + req.params.id);

    resourceDetailsModel.find({ "resourceId": req.params.id }, { resourceName: 1, _id: 0 }, function (err, data) {

        if (err) console.log('error')

        res.send(data);

    })

})
app.get('/getProjectNameById/:id', function (req, res) {

    //console.log("recieved data getProjectNameById : " + req.params.id);

    projectDetailsModel.find({ "id": req.params.id }, { text: 1, _id: 0 }, function (err, data) {

        if (err) console.log('error')
        //console.log('data--' + data)
        res.send(data);

    })

})

app.get('/getAllManagerDetails', function (req, res) {
  //  resourceDetailsModel.find({ "IsManagerFlag": true }, { resourceId: 1, resourceName: 1, resourceEmail: 1, _id: 0 }, function (err, data) {
      resourceDetailsModel. find({ "IsManagerFlag": true ,"resourceDesignation":{ $in: ["Engineering Manager","Manager" ] }}, 
            { resourceId: 1, resourceName: 1, resourceEmail: 1, _id: 0 }, function (err, data) {
     
        if (err) console.log('error')
        ////console.log("Manager Details : " +JSON.stringify(data)) ;
        res.send(data);

    })

})


//end of archana
